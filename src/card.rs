//! # Cards
//!
//! This module contains types to do with cards.

use std::path::PathBuf;

use chrono::{DateTime, Utc};
use serde::{Deserialize, Serialize};

/// A struct representing the card as a whole.
///
/// This includes the card details, as well as its local score, due date, and the path to its parent deck.
#[derive(Debug, Clone, Hash, Eq, PartialEq, PartialOrd, Ord, Serialize, Deserialize)]
pub struct Card {
    /// The content of the card
    pub inner: CardInner,
    /// The path to the deck that the card belongs to
    pub path: PathBuf,
    /// The score of a card
    pub score: usize,
    /// The due date of the card
    pub due_date: Option<DateTime<Utc>>,
}

/// A struct representing the serialisable information of a card
///
/// This contains the question, answer and input type of the card
#[derive(Debug, Clone, Hash, Eq, Serialize, Deserialize, PartialEq, PartialOrd, Ord)]
pub struct CardInner {
    /// The question of the [`Card`]
    pub question: String,
    /// The answer to the question of the [`Card`]
    pub answer: String,
    /// The type of card
    #[serde(default)]
    pub question_type: CardType,
}

impl From<CardInner> for Card {
    fn from(val: CardInner) -> Self {
        Card {
            inner: CardInner {
                question: val.question,
                answer: val.answer,
                question_type: val.question_type,
            },
            path: Default::default(),
            score: Default::default(),
            due_date: Default::default(),
        }
    }
}

impl From<Card> for CardInner {
    fn from(card: Card) -> Self {
        CardInner {
            question: card.inner.question,
            answer: card.inner.answer,
            question_type: card.inner.question_type,
        }
    }
}

impl AsRef<CardInner> for CardInner {
    fn as_ref(&self) -> &CardInner {
        self
    }
}

/// The input method of a card
#[derive(Debug, Clone, Serialize, Deserialize, Hash, Eq, PartialEq, PartialOrd, Ord)]
pub enum CardType {
    /// The default. Offers `j` and `k` functionality
    Memory,
    /// Shows an input box and asks you to input into the card.
    Input,
}

impl Default for CardType {
    fn default() -> Self {
        Self::Memory
    }
}
