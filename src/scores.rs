use std::borrow::Cow;

use chrono::{DateTime, Utc};
use serde::{Deserialize, Serialize};

use crate::app::App;
use crate::card::CardInner;

/// A scores struct which serde can deal with to
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Scores {
    /// The scores of each card
    #[serde(default)]
    pub scores: Vec<(CardInner, usize)>,
    /// The due date of each of the cards
    #[serde(default)]
    pub dues: Vec<(CardInner, DateTime<Utc>)>,
    /// The cards currently in circulation
    #[serde(default)]
    pub circulating: Vec<CardInner>,
}

impl<'a> From<&'a App> for Scores {
    fn from(app: &App) -> Self {
        Self {
            scores: {
                let mut scores = Vec::new();

                for card in app.cards() {
                    scores.push((Cow::Borrowed(card).inner.clone(), card.score));
                }

                scores
            },
            dues: {
                let mut dues = Vec::new();

                // Loops through all the dues and references
                // the card (if it exists) into the hashmap.
                for card in app.cards() {
                    dues.push((
                        Cow::Borrowed(card).inner.clone(),
                        card.due_date.unwrap_or(Utc::now()),
                    ));
                }

                dues
            },
            circulating: {
                let mut circulating = Vec::new();
                for card in app.circulating() {
                    if let Some(card) = app.cards().get(*card) {
                        circulating.push(Cow::Borrowed(card).inner.clone())
                    }
                }
                circulating
            },
        }
    }
}
