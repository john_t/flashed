use std::path::PathBuf;

/// The clap CLI options struct
#[derive(Debug, Clone)]
#[cfg_attr(feature = "clap", derive(clap::Parser))]
#[cfg_attr(feature = "serde_opts", derive(serde::Deserialize, serde::Serialize))]
pub struct Opts {
    /// The input file - in json
    pub input: PathBuf,
    /// If enabled all card scores will be reset
    #[cfg_attr(feature = "clap", clap(long, short))]
    pub reset: bool,
    /// Prevents the game from writing to a save
    #[cfg_attr(feature = "clap", clap(long, short))]
    pub nowrite: bool,
    /// How many cards to be testing on at once
    #[cfg_attr(feature = "clap", clap(long, short))]
    pub testing: Option<usize>,
    /// Should cards be set to 0 when demoted?
    #[cfg_attr(feature = "clap", clap(long, short))]
    pub zeroize: bool,
}
