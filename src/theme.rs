use tuviv::style::{Color, Modifier, Style};

pub const KEY: Style = Style {
    fg: None,
    bg: None,
    add_modifier: Modifier::BOLD,
    sub_modifier: Modifier::empty(),
};

pub const CARD_FLIPPED: Style = Style {
    fg: Some(Color::Blue),
    bg: None,
    add_modifier: Modifier::empty(),
    sub_modifier: Modifier::empty(),
};

pub const CARD: Style = Style {
    fg: Some(Color::Green),
    bg: None,
    add_modifier: Modifier::empty(),
    sub_modifier: Modifier::empty(),
};

pub const STAT: Style = Style {
    fg: None,
    bg: None,
    add_modifier: Modifier::BOLD,
    sub_modifier: Modifier::empty(),
};

pub const CORRECT: Style = Style {
    fg: Some(Color::Green),
    bg: None,
    add_modifier: Modifier::BOLD,
    sub_modifier: Modifier::empty(),
};

pub const INCORRECT: Style = Style {
    fg: Some(Color::Red),
    bg: None,
    add_modifier: Modifier::BOLD,
    sub_modifier: Modifier::empty(),
};

pub const YOUR_ANSWER: Style = Style {
    fg: None,
    bg: None,
    add_modifier: Modifier::ITALIC.union(Modifier::DIM),
    sub_modifier: Modifier::empty(),
};
