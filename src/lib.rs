#![doc = include_str!("../README.md")]
#![warn(missing_docs)]

mod card;
pub use card::{Card, CardType, CardInner};

mod deck;
pub use deck::Deck;
pub use deck::DeckError;

mod app;
pub use app::App;
pub use app::{CardIdx, DURATIONS};

mod scores;
pub use scores::Scores;

mod opts;
pub use opts::Opts;
