# Flashed! ![Flashed logo](https://gitlab.com/john_t/flashed/-/raw/master/icon.svg)

Flashed is a simple flashcard program written in rust. It also has an
associative library.

It takes a deck as input which is written in json:

```json
[
    {"question": "to, at", "answer": "ad" }
]
```

It contains simple controls.

After use it will save a adjacent file containing your progress:

```sh
$ flashed example.json
```

Would result in

```sh
$ ls

example.json
example.score.json
```
